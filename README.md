# ScottTheMeme's Elixir Docker Images

[![pipeline status](https://gitlab.com/ScottTheMeme/elixir-docker/badges/master/pipeline.svg)](https://gitlab.com/ScottTheMeme/elixir-docker/-/commits/master)

This is a repo designed to build any and all _(of my choosing)_ images **weekly** for all the latest security patches. _Also to get around the Docker Hub rate limits._

## Versions

| Default pipeline | 1.14 |
| ---------------- | ---- |

| Version     | Scheduled Pipeline? |
| ----------- | ------------------- |
| 1.14        | x                   |
| 1.13-otp-25 | x                   |
| 1.13        | x                   |
| 1.12        | x                   |

## Contributing

Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

## Pulling Images

The docker registry for this project is publicly available.

```
registry.gitlab.com/scottthememe/elixir-docker:x.y
```
